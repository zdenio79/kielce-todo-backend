import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Todo } from './todo/@types/entity/todo.entity';
import { TodoController } from './todo/todo.controller';
import { TodoService } from './todo/todo.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'szkolenie-todo',
      entities: [Todo],
      synchronize: true,
      // Logi z SQL-a - linijka poniżej
      // logging: "all", 
    }),
    TypeOrmModule.forFeature([Todo])
  ],
  
  controllers: [AppController, TodoController],
  providers: [AppService, TodoService],
})
export class AppModule {}
