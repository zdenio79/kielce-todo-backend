import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { AddTodoItemDto } from './@types/dto/addTodoItem.dto';
import { Todo } from './@types/entity/todo.entity';


@Injectable()
export class TodoService {
    constructor(@InjectRepository(Todo)
                private todoRepository: Repository<Todo>) {

    }

    public getTodoList(queryString: string = '', sortColumn: string = 'createdAt', sortOrder: string = 'DESC'): Promise<Todo[]> {
        const qb = this.todoRepository.createQueryBuilder('todo');

        if(queryString) {
            qb.where('todo.title LIKE :param', {param: `%${queryString}% `})
                .orWhere('todo.description LIKE :param', {param: `%${queryString}%`})
        }
        qb.orderBy(`todo.${sortColumn}`, sortOrder === 'ASC' ? 'ASC' : 'DESC');
        return qb.getMany();
    }
     
    public getTodoItem(todoId: string): Promise<Todo> {
        return this.todoRepository.findOne(todoId);
    }

    public addTodoItem(addTodoItemDto: AddTodoItemDto): Promise<Todo> {
        return this.todoRepository.save({
            title: addTodoItemDto.title,
            description: addTodoItemDto.description,
            completed: addTodoItemDto.completed || false,
        })
    }

    public async makeTodoItemComplete(todoId: string): Promise<void> {
        await this.todoRepository.update({
            id: todoId,
        }, {
            completed: true,
        });
    }

    public async makeTodoItemUncomplete(todoId: string): Promise<void> {
        await this.todoRepository.update({
            id: todoId,
        }, {
            completed: false,
        });
    }

    public async deleteTodoItemUncomplete(todoId: string): Promise<void> {
        await this.todoRepository.softDelete({
            id: todoId
        });
    }

}

